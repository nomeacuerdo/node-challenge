const homepage = (req, res) => {
  const templateConfig = {
    page: 'home',
    port: process.env.PORT,
  };

  res.render('../views/templates/homepage.nunjucks', templateConfig);
};

module.exports = homepage;
