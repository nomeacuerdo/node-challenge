const router = require('express').Router();

router.get('/', require('./homepage'));

module.exports = router;
