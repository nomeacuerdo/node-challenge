module.exports = {
  dump: obj => `<pre> ${JSON.stringify(obj)} </pre>`,
  // A poor man's object dump functionality, ex: {{ context | dump | safe }}
};